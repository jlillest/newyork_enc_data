# New York ENC GIS Data

This repo has three example ENC datasets from dives done in the St. Lawrence River
near Alexandria Bay.  The files here fall into two categories, Walls and QGIS.
  
## Walls Cave Survey Files
Walls is a cave survey data management tool 
(https://www.texasspeleologicalsurvey.org/software/walls/tsswalls.php) that will 
compile data, perform loop closures to distribute errors across a survey set, and 
export shapefiles for use in GIS applications.

Three walls survey files that were generated using the Seacraft KML conversion
tool (https://gitlab.com/jlillest/seacraft_converter) live here.  These files can
be tuned in Walls to fix anchor points, add waypoints, etc.  Walls will then 
generate shapefiles to be used in GIS software.

## QGIS Files
A sample QGIS project(v3.2) lives here that can be used to visualize the ENC runs on
aerial and topographic map tile sets.  This is similar to importing into Google Earth
or any other similar GIS application.  The KML files can then be compared easily
against post-processed data to compare drift and status of the data from the ENC.
